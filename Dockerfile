FROM maven:alpine

WORKDIR /home/project
COPY . /home/project
COPY wait-for-it.sh /bin/waitforit
CMD waitforit database:3306 -t 0 -- echo "Db is ready" && mvn test