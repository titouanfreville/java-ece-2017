# ECE JAVA project 2017

*Ece 4th Year JAVA project*

## Status

[![pipeline status](https://gitlab.com/titouanfreville/java-ece-2017/badges/master/pipeline.svg)](https://gitlab.com/titouanfreville/java-ece-2017/commits/master)

## Requirement

`docker` and `docker-compose`

OR

`maven` and `mysql` locally installed and set up

## Run test

`docker-compose run maven`

OR

`maven test`

by default, test are running for localhost.

## What is not working

Multiple booking for seat (should find a way to remove the issue with duplicate entry
caused by not getting a link between seat and wagon).