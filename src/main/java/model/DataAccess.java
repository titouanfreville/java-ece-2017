package model;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.sql.*;
import java.sql.Date;
import java.util.*;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.security.SecureRandom;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;
import java.io.*;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Provides the application with high-level methods to access the persistent
 * data store. The class hides the fact that data are stored in a RDBMS and also
 * hides all the complex SQL machinery required to access it.
 * <p>
 * The constructor and the methods of this class all throw a
 * {@link DataAccessException} whenever an unrecoverable error occurs, e.g. the
 * connexion to the database is lost.
 * <p>
 * <b>Note to the implementors</b>: You <b>must not</b> alter the interface of
 * this class' constructor and methods, including the exceptions thrown.
 *
 * @author Jean-Michel Busca
 */
public class DataAccess {
    /* {@link close} method is called.*/
    /**
     * Creates a new <code>DataAccess</code> object that interacts with the
     * specified database, using the specified login and password. Each object
     * maintains a <b>dedicated</b> connection to the database until the
     *
     *
     * @param url the url of the database to connect to
     * @param login the (application) login to use
     * @param password the password
     *
     * @throws DataAccessException if an unrecoverable error occurs
     */
    //private String URL = "jdbc:mysql://localhost:3306/ml3";
    //private String LOGIN = "root";
    //private String PASSWORD = "root";
    private Logger log = Logger.getLogger(this.getClass().getName());
    private ConsoleHandler cons_log = new ConsoleHandler();
    private SQLFileRunner runner = null;
    private String user = null;
    private String password = null;
    private String host = null;
    private String port = null;
    private String database = null;

    private Connection con = null;

    public Connection getConnection() {
        return this.con;
    }

    // If no arguments provided at object construction. User environement variables:
    // MYSQL_ROOT_PASSWORD as root password
    // MYSQL_DATABASE for database
    // MYSQL_USER as user
    // MYSQL_PASSWORD as user password
    // MYSQL_HOST as hostname
    // MYSQL_PORT as host port
    // If MYSQL_USER is not defined, it will used root by default.
    // Default root passwort is mysqlpassword
    // Default database is ece-train-2017
    // Default host is localhost on local and database in container
    // Default port is 3306
    public DataAccess() throws DataAccessException {
        this.cons_log.setLevel(Level.ALL);
        log.log(Level.INFO, "Initializing from environment");
        this.user = System.getenv("MYSQL_USER");
        if (this.user == null) {
            this.user = "root";
            log.log(Level.WARNING, "Using default user: " + this.user);
            this.password = System.getenv("MYSQL_ROOT_PASSWORD");
        } else {
            this.password = System.getenv("MYSQL_PASSWORD");
        }
        if (this.password == null) {
            this.password = "mysqlpassword";
            log.log(Level.WARNING, "Using default password: " + this.password);
        }
        this.database = System.getenv("MYSQL_DATABASE");
        if (this.database == null) {
            this.database = "ece-train-2017";
            this.log.log(Level.WARNING, "Using default database name: " + this.database);
        }
        this.host = System.getenv("MYSQL_HOST");
        if (this.host == null) {
            this.host = "localhost";
            String inDocker = System.getenv("IN_DOCKER");
            if (inDocker != null) {
                this.log.log(Level.WARNING, "In Docker environment");
                this.host = "database";
            }
            this.log.log(Level.WARNING, "Using default host name: " + this.host);
        }
        this.port = System.getenv("MYSQL_PORT");
        if (this.port == null) {
            this.port = "3306";
            this.log.log(Level.WARNING, "Using default port: " + this.port);
        }
        String url = "jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database;
        try {
            this.con = DriverManager.getConnection(url, this.user, this.password);
            this.runner  = new SQLFileRunner(this.con, true, true);
        } catch (SQLException sqlex) {
            String error = "Error: "+sqlex.getSQLState()+"\nMessage: "+sqlex.getMessage()+"\nState: "+sqlex.getErrorCode();
            this.log.log(Level.SEVERE, error);
            throw new DataAccessException(error);
        }
        this.log.log(Level.INFO, "Connected");

    }

    public DataAccess(String url, String user, String password) throws DataAccessException {
        this.cons_log.setLevel(Level.ALL);
        try {
            this.con = DriverManager.getConnection(url, user, password);
            this.runner  = new SQLFileRunner(this.con, false, true);
        } catch (SQLException sqlex) {
            String error = "Error: "+sqlex.getSQLState()+"\nMessage: "+sqlex.getMessage()+"\nState: "+sqlex.getErrorCode();
            this.log.log(Level.SEVERE, error);
            throw new DataAccessException(error);
        }
        this.log.log(Level.INFO, "Connected");
    }

    public void initDatabase() throws DataAccessException {
        try {
            String current = new java.io.File( "." ).getCanonicalPath();
            Path path = Paths.get(current, "database.sql");
            path.normalize();
            initDatabase(path.toString());
        } catch (IOException e) {
            String error = "Error: "+e.getMessage();
            this.log.log(Level.SEVERE, error);
            throw new DataAccessException(error);
        }
    }

    /**
     * Creates and populates the database according to all the examples provided
     * in the requirements of marked lab 2. If the database already exists
     * before the method is called, the method discards the database and creates
     * it again from scratch.
     *
     * @param sqlScriptPath
     *                  - path to the initialization script
     *
     * @throws DataAccessException if an unrecoverable error occurs
     */
    public void initDatabase(String sqlScriptPath) throws DataAccessException {
        if (this.con == null) {
            String error = "Error: No Connection sat up. Please check that DataAccess worked as Intended";
            this.log.log(Level.SEVERE, error);
            throw new DataAccessException(error);
        }
        // Run init script
        try {
            this.runner.runScript(sqlScriptPath);
        } catch (Exception e) {
            String error = "Error: "+e.getMessage();
            this.log.log(Level.SEVERE, error);
            throw new DataAccessException(error);
        }
    }

    /**
     * Creates and populates the database according to all the examples provided
     * in the requirements of marked lab 2. If the database already exists
     * before the method is called, the method discards the database and creates
     * it again from scratch.
     *
     * @param cleanScriptPath
     *                  - path to the clean script
     *
     * @param sqlScriptPath
     *                  - path to the initialization script
     *
     * @throws DataAccessException if an unrecoverable error occurs
     */
    public void initDatabase(String sqlScriptPath, String cleanScriptPath) throws DataAccessException {
        try {
            initDatabase(sqlScriptPath);
            this.runner.runScript(cleanScriptPath);
        } catch (Exception e) {
            String error = "Error: "+e.getMessage();
            this.log.log(Level.SEVERE, error);
            throw new DataAccessException(error);
        }
    }

    /**
     * See Operation 2.1.1.
     *
     * @param departureStation
     * @param arrivalStation
     * @param fromDate
     * @param toDate
     *
     * @return the list of journey going from `departureStation` to `arrivalStation` taking place between `fromDate` and `toDate`, including the empty list if
     * no journey is found
     *
     * @throws DataAccessException if an unrecoverable error occurs
     */
    public List<Journey> getTrainTimes(String departureStation, String arrivalStation, Date fromDate, Date toDate)
        throws DataAccessException {
        PreparedStatement statementGetDep = null;
        PreparedStatement statementGetFinal = null;
        ResultSet rs = null;
        ResultSet frs = null;
        try {
            String getDepTrain = "SELECT id_train, departure, arrival, line_composition.id_line " +
                    "FROM trip NATURAL JOIN train NATURAL JOIN line NATURAL JOIN line_composition " +
                    "WHERE trip.departure BETWEEN ? AND ? AND trip.id_station = ?";
            String getTrains =  "SELECT id_train " +
                    "FROM train NATURAL JOIN line NATURAL JOIN line_composition " +
                    "WHERE id_train = ? AND line_composition.id_station = ? AND line_composition.station_deserved_number != 0;";
            statementGetDep = this.con.prepareStatement(getDepTrain);
            statementGetFinal = this.con.prepareStatement(getTrains);
            statementGetDep.setDate(1, fromDate);
            statementGetDep.setDate(2, toDate);
            statementGetDep.setString(3, departureStation);
            rs = statementGetDep.executeQuery();
            Journey journey;
            List<Journey> res = new ArrayList<>();
            while (rs.next()) {
                statementGetFinal.setInt(1, rs.getInt(1));
                statementGetFinal.setString(2, arrivalStation);
                frs = statementGetFinal.executeQuery();
                while (frs.next()) {
                    journey = new Journey(departureStation, arrivalStation, rs.getInt(1), rs.getDate(2), rs.getDate(3) );
                    res.add(journey);
                }
            }
            return res;
        } catch (SQLException e) {
            String error = "Error: "+e.getMessage();
            this.log.log(Level.SEVERE, error);
            throw new DataAccessException(error);
        } finally {
            try {
                if (statementGetDep != null) {
                    statementGetDep.close();
                }
                if (statementGetFinal != null) {
                    statementGetFinal.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (frs != null) {
                    frs.close();
                }
            } catch (SQLException e) {
                String error = "Error: "+e.getMessage();
                this.log.log(Level.SEVERE, error);
                throw new DataAccessException(error);
            }

        }
    }

    /**
     * See Operation 2.1.2
     *
     * @param departureStation
     * @param arrivalStation
     * @param travelPeriod
     * @param passengerCount
     * @param travelClass
     *
     * @return the bought ticket, or <code>null</code> if some parameter was
     * incorrect
     *
     * @throws DataAccessException if an unrecoverable error occurs
     */
    public Ticket buyTicket(String departureStation, String arrivalStation,
                            Period travelPeriod, int passengerCount, Class travelClass)
            throws DataAccessException {
        List<Journey> journeys = null;
        List<Journey> emptyCheck = new ArrayList<>();
        PreparedStatement checkPeriodStatement = null;
        ResultSet checkPeriod = null;
        try {
            Calendar plusTwo = Calendar.getInstance();
            plusTwo.add(Calendar.MONTH, 2);
            journeys = getTrainTimes(departureStation, arrivalStation, new Date(Calendar.getInstance().getTimeInMillis()), new Date(plusTwo.getTimeInMillis()));
            if (journeys == null || journeys == emptyCheck) {
                log.log(Level.WARNING, "No journey available between: " + departureStation + " and " + arrivalStation);
                return null;
            }
            String checkPeriodQuery = "SELECT period " +
                    "FROM trip " +
                    "WHERE id_train = ? AND id_station = ?";
            boolean okPeriod = false;
            checkPeriodStatement = this.con.prepareStatement(checkPeriodQuery);
            for (Journey j : journeys) {
                checkPeriodStatement.setInt(1, j.getTrainNumber());
                checkPeriodStatement.setString(2, j.getDepartureStation());
                checkPeriod = checkPeriodStatement.executeQuery();
                while (checkPeriod.next()) {
                    okPeriod = okPeriod || (checkPeriod.getString(1).equalsIgnoreCase(travelPeriod.toString()));
                }
            }
            if (!okPeriod) {
                log.log(Level.WARNING, "No journey available between: " + departureStation + " and " + arrivalStation + " using period: "+travelPeriod.toString());
                return null;
            }
            return new Ticket(departureStation, arrivalStation, travelPeriod, passengerCount, travelClass);
        } catch (DataAccessException e) {
            String error = "Error: " + e.getMessage();
            this.log.log(Level.SEVERE, error);
            throw new DataAccessException(error);
        } catch (SQLException e) {
            String error = "Error: " + e.getMessage();
            this.log.log(Level.SEVERE, error);
            throw new DataAccessException(error);
        } finally {
            try {
                if (checkPeriodStatement != null) {
                    checkPeriodStatement.close();
                }
                if (checkPeriod != null) {
                    checkPeriod.close();
                }
            } catch (SQLException e) {
                String error = "Error: "+e.getMessage();
                this.log.log(Level.SEVERE, error);
                throw new DataAccessException(error);
            }
        }
    }

    /**
     * See Operation 2.1.3.
     *
     * @param trainNumber
     * @param departureDate
     * @param departureStation
     * @param arrivalStation
     * @param passengerCount
     * @param travelClass
     * @param customerEmail
     *
     * @return the booking, or <code>null</code> if some parameter was incorrect
     * or not enough seats were available
     *
     * @throws DataAccessException if an unrecoverable error occurs
     */
    public Booking buyTicketAndBook(int trainNumber, Date departureDate, String departureStation, String arrivalStation, int passengerCount, Class travelClass, String customerEmail)
        throws DataAccessException {
        // Lists and objects
        List<Journey> journeys = null;
        List<Journey> emptyCheck = new ArrayList<>();
        ResultSet checkPeriod = null;
        Period travelPeriod = getPeriodFromDate(departureDate);
        // Statements
        PreparedStatement checkPeriodStatement = null;
        PreparedStatement reservationStatement = null;
        PreparedStatement getTripStationsStatement = null;
        PreparedStatement getTripLengthStatement = null;
        PreparedStatement getPeriodMajorationStatement = null;
        PreparedStatement getClassKmTarificationStatement = null;
        PreparedStatement getClassSeatsStatement = null;
        PreparedStatement bookSeatStatement = null;
        // Result set
        ResultSet getTripStations = null;
        ResultSet getTripLength = null;
        ResultSet getPeriodMajoration = null;
        ResultSet getClassKmTarification = null;
        ResultSet getClassSeats = null;
        int reservation = -1;
        int bookSeat = -1;

        try {
            Calendar plusTwo = Calendar.getInstance();
            plusTwo.add(Calendar.MONTH, 2);
            journeys = getTrainTimes(departureStation, arrivalStation, new Date(Calendar.getInstance().getTimeInMillis()), new Date(plusTwo.getTimeInMillis()));
            if (journeys == null || journeys == emptyCheck) {
                log.log(Level.WARNING, "No journey available between: " + departureStation + " and " + arrivalStation);
                return null;
            }
            String checkPeriodQuery = "SELECT period " +
                    "FROM trip " +
                    "WHERE id_train = ? AND id_station = ?";
            boolean okPeriod = false;
            checkPeriodStatement = this.con.prepareStatement(checkPeriodQuery);
            for (Journey j : journeys) {
                checkPeriodStatement.setInt(1, j.getTrainNumber());
                checkPeriodStatement.setString(2, j.getDepartureStation());
                checkPeriod = checkPeriodStatement.executeQuery();
                while (checkPeriod.next()) {
                    okPeriod = okPeriod || (checkPeriod.getString(1).equalsIgnoreCase("BLUE"));
                }
            }
            if (!okPeriod) {
                log.log(Level.WARNING, "No journey available between: " + departureStation + " and " + arrivalStation + " using period: "+"Blue");
                return null;
            }

            String getTripStationsQuery = "SELECT id_station FROM line_composition " +
                    "NATURAL JOIN line NATURAL JOIN train WHERE id_train = ? " +
                    "ORDER BY station_deserved_number";
            getTripStationsStatement = this.con.prepareStatement(getTripStationsQuery);
            getTripStationsStatement.setInt(1, trainNumber);
            getTripStations = getTripStationsStatement.executeQuery();
            boolean save = false;
            List<String> Full = new ArrayList<>();
            while (getTripStations.next()) {
                String cur = getTripStations.getString(1);
                if (cur.equalsIgnoreCase(departureStation) || cur.equalsIgnoreCase(arrivalStation)) {
                    save = !save;
                    Full.add(cur);
                    continue;
                }
                if (save) {
                    Full.add(cur);
                }
            }
            if (Full == null || Full == (new ArrayList<String>())) {
                log.log(Level.WARNING, "Error: The trip does not have any line to support it");
                return null;
            }
            String getTripLengthQuery = "SELECT distance FROM distance " +
                    "WHERE (station1 LIKE ? AND station2 LIKE ?) OR (station1 LIKE ? AND station2 LIKE ?)";
            getTripLengthStatement = this.con.prepareStatement(getTripLengthQuery);
            String previousStation = Full.get(0);
            Full.remove(0);
            Float tripLength = (float)0;
            for (String station : Full) {
                getTripLengthStatement.setString(1, previousStation);
                getTripLengthStatement.setString(4, previousStation);
                getTripLengthStatement.setString(2, station);
                getTripLengthStatement.setString(3, station);
                getTripLength = getTripLengthStatement.executeQuery();
                previousStation = station;
                if (getTripLength.first()) {
                    tripLength = tripLength + getTripLength.getFloat(1); // should only have 1 result
                } else {
                    log.log(Level.SEVERE, "No value");
                    return null;
                }
            }
            String getClassKmTarificationQuery = "SELECT tarification_km FROM class WHERE class = ?";
            String getPeriodMajorationQuery = "SELECT majoration FROM period WHERE period = ?";
            getClassKmTarificationStatement = this.con.prepareStatement(getClassKmTarificationQuery);
            getPeriodMajorationStatement = this.con.prepareStatement(getPeriodMajorationQuery);
            String classVal = travelClass.toString();
            String periodVal = travelPeriod.toString();
            getClassKmTarificationStatement.setString(1, classVal.toLowerCase());
            getPeriodMajorationStatement.setString(1, periodVal.toLowerCase());
            getClassKmTarification = getClassKmTarificationStatement.executeQuery();
            getPeriodMajoration = getPeriodMajorationStatement.executeQuery();
            Float price = (float)0;
            if (getPeriodMajoration.first() && getClassKmTarification.first()) {
                Float majoration = getPeriodMajoration.getFloat(1);
                Float kmTarification = getClassKmTarification.getFloat(1);
                Float initialPrice = (kmTarification * tripLength);
                price = initialPrice * (1+majoration/100) * passengerCount;
            } else {
                log.log(Level.SEVERE, "No value");
                return null;
            }

            List<Integer> avSeats = getAvailableSeats(trainNumber, departureDate, departureStation, arrivalStation);
            if (avSeats.isEmpty()) {
                log.log(Level.SEVERE, "No seats");
                return null;
            }

            // Finally book :)
            String idBooking = new RandomString().nextString();
            String reservationQuery = "INSERT INTO booking " +
                    "(id_booking, email, arrival, passenger_count) " +
                    "VALUES (?, ?, ?, ?)";
            reservationStatement = this.con.prepareStatement(reservationQuery);
            reservationStatement.setString(1, idBooking);
            reservationStatement.setString(2, customerEmail);
            reservationStatement.setString(3, arrivalStation);
            reservationStatement.setInt(4, passengerCount);
            reservation = reservationStatement.executeUpdate();
            if (reservation < 1) {
                log.log(Level.WARNING, "Reservation failed");
                return null;
            }
            // book seat
            String getClassSeatsQuery = "SELECT id_seat, wagon.id_wagon " +
                    "FROM seat NATURAL JOIN wagon_kind NATURAL JOIN wagon NATURAL JOIN train " +
                    "WHERE train.id_train = ? AND wagon_kind.class = ? ";

            getClassSeatsStatement = this.con.prepareStatement(getClassSeatsQuery);
            getClassSeatsStatement.setInt(1, trainNumber);
            getClassSeatsStatement.setString(2, classVal);
            getClassSeats = getClassSeatsStatement.executeQuery();
            List<List<Integer>> classSeats = new ArrayList<>();
            while (getClassSeats.next()) {
                List<Integer> tmpList = new ArrayList<>();
                if (avSeats.contains(getClassSeats.getInt(1))) {
                    tmpList.add(getClassSeats.getInt(1));
                    tmpList.add(getClassSeats.getInt(2));
                    classSeats.add(tmpList);
                }
            }

            int i;
            String bookSeatQuery = "INSERT INTO booking_seat (id_booking, id_seat, id_wagon) " +
                    "VALUES (?,?,?)";
            bookSeatStatement = this.con.prepareStatement(bookSeatQuery);
            bookSeatStatement.setString(1, idBooking);
            List<Integer> bookedSeat = new ArrayList<>();
            for (i=0; i<passengerCount; i++) {
                List<Integer> tmp = classSeats.get(i);
                bookSeatStatement.setInt(2, tmp.get(0));
                bookSeatStatement.setInt(3, tmp.get(1));
                bookSeat = bookSeatStatement.executeUpdate();
                bookedSeat.add(tmp.get(0));
                if (bookSeat < 1) {
                    log.log(Level.WARNING, "Could not book seats");
                }
                bookSeat = -1;
            }
            return new Booking(idBooking, customerEmail, price, departureDate, bookedSeat);
        } catch (DataAccessException e) {
            String error = "Error: " + e.getMessage();
            this.log.log(Level.SEVERE, error);
            throw new DataAccessException(error);
        } catch (SQLException e) {
            String error = "Error: " + e.getMessage();
            this.log.log(Level.SEVERE, error);
            throw new DataAccessException(error);
        }  finally {
            try {
                if (checkPeriodStatement != null) {
                    checkPeriodStatement.close();
                }
                if (checkPeriod != null) {
                    checkPeriod.close();
                }
                if (reservationStatement != null) {
                    reservationStatement.close();
                }
            } catch (SQLException e) {
                String error = "Error: "+e.getMessage();
                this.log.log(Level.SEVERE, error);
                throw new DataAccessException(error);
            }
        }
    }

    /**
     * See Operation 2.1.4
     *
     * @param bookingID
     * @param customerEmail
     *
     * @return <code>true</code> if the booking was cancelled, and
     * <code>false</code> otherwise
     *
     * @throws DataAccessException if an unrecoverable error occurs
     */
    public boolean cancelBooking(String bookingID, String customerEmail)
            throws DataAccessException {
        String deleteQuery = "DELETE FROM booking WHERE id_booking like ? AND email like ?;";
        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = this.con.prepareStatement(deleteQuery);
            deleteStatement.setString(1, bookingID);
            deleteStatement.setString(2, customerEmail);
            int deleted = deleteStatement.executeUpdate();
            return deleted == 1;
        } catch (SQLException e) {
            String error = "Error: "+e.getMessage();
            this.log.log(Level.SEVERE, error);
            throw new DataAccessException(error);
        }
    }

    /**
     * See Operation 2.2.2
     *
     * @param trainNumber
     * @param departureDate
     * @param beginStation
     * @param endStation
     *
     * @return the list of available seats, including the empty list if no seat
     * is available
     *
     * @throws DataAccessException if an unrecoverable error occurs
     */
   public List<Integer> getAvailableSeats(int trainNumber, Date departureDate, String beginStation, String endStation)
        throws DataAccessException { // Lists and objects
       List<Journey> journeys = null;
       ResultSet checkPeriod = null;
       Period travelPeriod = getPeriodFromDate(departureDate);
       // Statements
       PreparedStatement checkPeriodStatement = null;
       PreparedStatement getAllCheckTripStatement = null;
       PreparedStatement getTripStationsStatement = null;
       PreparedStatement getAllCheckBookStatement = null;
       PreparedStatement getBookedSeatsStatement = null;
       PreparedStatement getAllSeatsStatement = null;
       // Result set
       ResultSet getTripStations = null;
       ResultSet getAllCheckTrip = null;
       ResultSet getAllCheckBook = null;
       ResultSet getBookedSeats = null;
       ResultSet getAllSeats = null;

       try {
           Calendar plusOne = Calendar.getInstance();
           plusOne.setTime(departureDate);
           plusOne.add(Calendar.DATE, 1);
           Calendar minusOne = Calendar.getInstance();
           minusOne.setTime(departureDate);
           minusOne.add(Calendar.DATE, -1);
           journeys = getTrainTimes(beginStation, endStation, new Date(minusOne.getTimeInMillis()), new Date(plusOne.getTimeInMillis()));
           if (journeys == null || journeys.isEmpty()) {
               log.log(Level.WARNING, "No journey available between: " + beginStation + " and " + endStation);
               return null;
           }
           String checkPeriodQuery = "SELECT period " +
                   "FROM trip " +
                   "WHERE id_train = ? AND id_station = ?";
           boolean okPeriod = false;
           checkPeriodStatement = this.con.prepareStatement(checkPeriodQuery);
           for (Journey j : journeys) {
               checkPeriodStatement.setInt(1, trainNumber);
               checkPeriodStatement.setString(2, beginStation);
               checkPeriod = checkPeriodStatement.executeQuery();
               while (checkPeriod.next()) {
                   String period = travelPeriod.toString().toLowerCase();
                   okPeriod = okPeriod || (checkPeriod.getString(1).equalsIgnoreCase(period));

               }
           }
           if (!okPeriod) {
               log.log(Level.WARNING, "No journey available between: " + beginStation + " and " + endStation + " using period: "+"Blue");
               return null;
           }

           String getTripStationsQuery = "SELECT id_station FROM line_composition " +
                   "NATURAL JOIN line NATURAL JOIN train WHERE id_train = ? " +
                   "ORDER BY station_deserved_number";
           getTripStationsStatement = this.con.prepareStatement(getTripStationsQuery);
           getTripStationsStatement.setInt(1, trainNumber);
           getTripStations = getTripStationsStatement.executeQuery();
           boolean save = false;
           List<String> Full = new ArrayList<>();
           while (getTripStations.next()) {
               String cur = getTripStations.getString(1);
               if (cur.equalsIgnoreCase(beginStation) || cur.equalsIgnoreCase(endStation)) {
                   save = !save;
                   Full.add(cur);
                   continue;
               }
               if (save) {
                   Full.add(cur);
               }
           }
           if (Full == null || Full == (new ArrayList<String>())) {
               log.log(Level.WARNING, "Error: The trip does not have any line to support it");
               return null;
           }

           List<String> stationStore = new ArrayList<>();
           List<String> toCheckStation = new ArrayList<>();
           boolean inRange = false;
           for (String j : Full) {
               if (j.equalsIgnoreCase(beginStation)) {
                   inRange = !inRange;
                   toCheckStation.add(j);
                   toCheckStation.addAll(stationStore);
               }
               if (j.equalsIgnoreCase(endStation)) {
                   inRange = !inRange;
                   stationStore = new ArrayList<>();
                   toCheckStation.add(j);
               }
               if (inRange) {
                   toCheckStation.add(j);
               } else {
                   stationStore.add(j);
               }
           }

           String getAllCheckTripQuery = "SELECT id_trip FROM trip WHERE id_train = ? AND id_station = ?";
           getAllCheckTripStatement = this.con.prepareStatement(getAllCheckTripQuery);
           getAllCheckTripStatement.setInt(1, trainNumber);

           List<Integer> tripIdList = new ArrayList<>();
           for (String station : toCheckStation) {
               getAllCheckTripStatement.setString(2, station);
               getAllCheckTrip = getAllCheckTripStatement.executeQuery();
               if (getAllCheckTrip.first()) {
                  tripIdList.add(getAllCheckTrip.getInt(1));
               }
           }

           String getAllCheckBookQuery = "SELECT id_booking FROM booking WHERE id_trip = ?";
           getAllCheckBookStatement = this.con.prepareStatement(getAllCheckBookQuery);
           List<String> bookingIdList = new ArrayList<>();
           for (Integer i : tripIdList) {
               getAllCheckBookStatement.setInt(1, i);
               getAllCheckBook = getAllCheckBookStatement.executeQuery();
               while (getAllCheckBook.next()) {
                   bookingIdList.add(getAllCheckBook.getString(1));
               }
           }

           List<Integer> bookedSeatList = new ArrayList<>();
           if (bookingIdList.size() < 1) {
               log.log(Level.WARNING, "Empty booking for now");
           } else {
               String getBookedSeatsQuery = "SELECT id_seat FROM booking_seat WHERE id_booking in ?";
               getBookedSeatsStatement = this.con.prepareStatement(getBookedSeatsQuery);
               String [] s = new String[bookingIdList.size()];
               s = bookingIdList.toArray(s);
               Array exArray = this.con.createArrayOf("VARCHAR", s);
               getBookedSeatsStatement.setArray(1, exArray);
               getBookedSeats = getBookedSeatsStatement.executeQuery();

               while (getBookedSeats.next()) {
                   bookedSeatList.add(getBookedSeats.getInt(1));
               }
           }

           String getAllSeatsQuery = "SELECT id_seat " +
                   "FROM seat NATURAL JOIN wagon_kind NATURAL JOIN wagon " +
                   "WHERE id_train = ? AND period like ?";
           getAllSeatsStatement = this.con.prepareStatement(getAllSeatsQuery);
           getAllSeatsStatement.setInt(1, trainNumber);
           getAllSeatsStatement.setString(2, travelPeriod.toString().toLowerCase());
           getAllSeats = getAllSeatsStatement.executeQuery();
           List<Integer> allSeatList = new ArrayList<>();
           while (getAllSeats.next()) {
               allSeatList.add(getAllSeats.getInt(1));
           }

           if (!bookedSeatList.isEmpty() && allSeatList.removeAll(bookedSeatList)) {
               return allSeatList;
           }return allSeatList;

       } catch (DataAccessException e) {
            String error = "Error: "+e.getMessage();
            this.log.log(Level.WARNING, error);
            throw new DataAccessException(error);
       } catch (SQLException e) {
           String error = "Error: "+e.getMessage();
           this.log.log(Level.WARNING, error);
           throw new DataAccessException(error);
       }
   }

   public Period getPeriodFromDate(Date date) {
        PreparedStatement statement = null;
        ResultSet res = null;

       try {
           String query = "SELECT * FROM  period_calendar";
           statement = this.con.prepareStatement(query);
           res = statement.executeQuery();
           while (res.next()) {
               if (date.after(res.getDate(2)) && date.before(res.getDate(3))) {
                   Period per = Period.BLUE;
                   switch (res.getString(1)) {
                       case "white":
                           per = Period.WHITE;
                           break;
                       case "WHITE":
                           per = Period.WHITE;
                           break;
                       case "red":
                           per = Period.RED;
                           break;
                       case "RED":
                           per = Period.RED;
                           break;
                       default:
                           break;
                   }
                   return per;
               }
           }
           log.log(Level.WARNING, "Date does not correspond to any period. Defaulting to Blue");
           return Period.BLUE;
       } catch (SQLException e) {
           String error = "Error: "+e.getMessage();
           this.log.log(Level.WARNING, error);
           return Period.BLUE;

       } finally {
           try {
               if (statement != null) {
                   statement.close();
               }
               if (res != null) {
                   res.close();
               }
           } catch (SQLException e) {
               String error = "Error: "+e.getMessage();
               this.log.log(Level.WARNING, error);
           }
       }
    }

    /**
     * Closes the underlying connection and releases all related ressources. The
     * application must call this method when it is done accessing the data
     * store.
     *
     * @throws DataAccessException if an unrecoverable error occurs
     */
   public void close() throws DataAccessException {
        try {
            this.con.close();
            this.con = null;
        } catch (SQLException e) {
            String error = "Error: "+e.getSQLState()+"\nMessage: "+e.getMessage()+"\nState: "+e.getErrorCode();
            this.log.log(Level.SEVERE, error);
            throw new DataAccessException(error);
        }
   }

}

/*
 * Slightly modified version of the com.ibatis.common.jdbc.SQLFileRunner class
 * from the iBATIS Apache project. Only removed dependency on Resource class
 * and a constructor
 */
/*
 *  Copyright 2004 Clinton Begin
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


/**
 * Tool to run database scripts
 */
class SQLFileRunner {
    private Logger log = Logger.getLogger(this.getClass().getName());

    private static final String DEFAULT_DELIMITER = ";";

    private Connection connection;

    private boolean stopOnError;
    private boolean autoCommit;

    private PrintWriter logWriter = new PrintWriter(System.out);
    private PrintWriter errorLogWriter = new PrintWriter(System.err);

    private String delimiter = DEFAULT_DELIMITER;
    private boolean fullLineDelimiter = false;

    /**
     * Default constructor
     */
    public SQLFileRunner(Connection connection, boolean autoCommit,
                         boolean stopOnError) {
        this.connection = connection;
        this.autoCommit = autoCommit;
        this.stopOnError = stopOnError;
    }

    public void setDelimiter(String delimiter, boolean fullLineDelimiter) {
        this.delimiter = delimiter;
        this.fullLineDelimiter = fullLineDelimiter;
    }

    /**
     * Setter for logWriter property
     *
     * @param logWriter
     *            - the new value of the logWriter property
     */
    public void setLogWriter(PrintWriter logWriter) {
        this.logWriter = logWriter;
    }

    /**
     * Setter for errorLogWriter property
     *
     * @param errorLogWriter
     *            - the new value of the errorLogWriter property
     */
    public void setErrorLogWriter(PrintWriter errorLogWriter) {
        this.errorLogWriter = errorLogWriter;
    }

    /**
     * Runs an SQL script using its path
     *
     * @param path
     *          - path of the source script on the computer
     */
    public void runScript(String path) throws IOException, SQLException {
        File script = new File(path);
        try {
            Reader reader = new FileReader(script);
            runScript(reader);
        } catch (SQLException e) {
            String error = "Error: "+e.getMessage();
            this.log.log(Level.SEVERE, error);
            throw e;
        } catch (IOException e) {
            String error = "Error: "+e.getMessage();
            this.log.log(Level.SEVERE, error);
            throw e;
        } catch (Exception e) {
            throw new RuntimeException("Error running script.  Cause: " + e, e);
        }
    }

    /**
     * Runs an SQL script (read in using the Reader parameter)
     *
     * @param reader
     *            - the source of the script
     */
    public void runScript(Reader reader) throws IOException, SQLException {
        try {
            boolean originalAutoCommit = connection.getAutoCommit();
            try {
                if (originalAutoCommit != this.autoCommit) {
                    connection.setAutoCommit(this.autoCommit);
                }
                runScript(connection, reader);
            } finally {
                connection.setAutoCommit(originalAutoCommit);
            }
        } catch (IOException e) {
            throw e;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException("Error running script.  Cause: " + e, e);
        }
    }

    /**
     * Runs an SQL script (read in using the Reader parameter) using the
     * connection passed in
     *
     * @param conn
     *            - the connection to use for the script
     * @param reader
     *            - the source of the script
     * @throws SQLException
     *             if any SQL errors occur
     * @throws IOException
     *             if there is an error reading from the Reader
     */
    private void runScript(Connection conn, Reader reader) throws IOException,
            SQLException, Error {
        StringBuffer command = null;
        try {
            LineNumberReader lineReader = new LineNumberReader(reader);
            String line = null;
            while ((line = lineReader.readLine()) != null) {
                if (command == null) {
                    command = new StringBuffer();
                }
                String trimmedLine = line.trim();
                if (trimmedLine.startsWith("--")) {
                    println(trimmedLine);
                } else if (trimmedLine.length() < 1
                        || trimmedLine.startsWith("//")
                        || trimmedLine.startsWith("--")) {
                    // Do nothing
                } else if ((!fullLineDelimiter
                        && trimmedLine.endsWith(getDelimiter()))
                        || (fullLineDelimiter
                        && trimmedLine.equals(getDelimiter()))) {
                    command.append(line.substring(0, line
                            .lastIndexOf(getDelimiter())));
                    command.append(" ");
                    Statement statement = conn.createStatement();

                    println(command);

                    boolean hasResults = false;
                    if (stopOnError) {
                        hasResults = statement.execute(command.toString());
                    } else {
                        try {
                            statement.execute(command.toString());
                        } catch (SQLException e) {
                            e.fillInStackTrace();
                            printlnError("Error executing: " + command);
                            printlnError(e);
                        }
                    }

                    if (autoCommit && !conn.getAutoCommit()) {
                        conn.commit();
                    }

                    ResultSet rs = statement.getResultSet();
                    if (hasResults && rs != null) {
                        ResultSetMetaData md = rs.getMetaData();
                        int cols = md.getColumnCount();
                        for (int i = 0; i < cols; i++) {
                            String name = md.getColumnLabel(i);
                            print(name + "\t");
                        }
                        println("");
                        while (rs.next()) {
                            for (int i = 0; i < cols; i++) {
                                String value = rs.getString(i);
                                print(value + "\t");
                            }
                            println("");
                        }
                    }

                    command = null;
                    try {
                        statement.close();
                    } catch (Exception e) {
                        // Ignore to workaround a bug in Jakarta DBCP
                    }
                    Thread.yield();
                } else {
                    command.append(line);
                    command.append(" ");
                }
            }
            if (!autoCommit) {
                conn.commit();
            }
        } catch (SQLException e) {
            e.fillInStackTrace();
            printlnError("Error executing: " + command);
            printlnError(e);
            throw e;
        } catch (IOException e) {
            e.fillInStackTrace();
            printlnError("Error executing: " + command);
            printlnError(e);
            throw e;
        } finally {
            conn.rollback();
            flush();
        }
    }

    private String getDelimiter() {
        return delimiter;
    }

    private void print(Object o) {
        if (logWriter != null) {
            System.out.print(o);
        }
    }

    private void println(Object o) {
        if (logWriter != null) {
            logWriter.println(o);
        }
    }

    private void printlnError(Object o) {
        if (errorLogWriter != null) {
            errorLogWriter.println(o);
        }
    }

    private void flush() {
        if (logWriter != null) {
            logWriter.flush();
        }
        if (errorLogWriter != null) {
            errorLogWriter.flush();
        }
    }
}

class RandomString {

    /**
     * Generate a random string.
     */
    public String nextString() {
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = symbols[random.nextInt(symbols.length)];
        return new String(buf);
    }

    public static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static final String lower = upper.toLowerCase(Locale.ROOT);

    public static final String digits = "0123456789";

    public static final String alphanum = upper + lower + digits;

    private final Random random;

    private final char[] symbols;

    private final char[] buf;

    public RandomString(int length, Random random, String symbols) {
        if (length < 1) throw new IllegalArgumentException();
        if (symbols.length() < 2) throw new IllegalArgumentException();
        this.random = Objects.requireNonNull(random);
        this.symbols = symbols.toCharArray();
        this.buf = new char[length];
    }

    /**
     * Create an alphanumeric string generator.
     */
    public RandomString(int length, Random random) {
        this(length, random, alphanum);
    }

    /**
     * Create an alphanumeric strings from a secure generator.
     */
    public RandomString(int length) {
        this(length, new SecureRandom());
    }

    /**
     * Create a random ticker id.
     */
    public RandomString() {
        this(6);
    }

}
