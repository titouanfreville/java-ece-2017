package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Defines a booking: the customer who made it, the total amount, the order
 * date, and the number of booked seats.
 *
 * @author Jean-Michel Busca
 */
public class Booking {

    private final String idBooking;
    private final String customer;
    private final float amount;
    private final Date date;
    private final List<Integer> seats;

    public Booking(String idBooking, String customer, float amount, Date date, List<Integer> seats) {
        this.idBooking = idBooking;
        this.customer = customer;
        this.amount = amount;
        this.date = date;
        this.seats = new ArrayList<>(seats);
    }

    @Override
    public String toString() {
        return "BookingInfo{"  + "id=" + idBooking + ", customer=" + customer + ", amount=" + amount + ", date=" + date + ", seats=" + seats + '}';
    }

    public String getId() { return idBooking;}

    public String getCustomer() {
        return customer;
    }

    public float getAmount() {
        return amount;
    }

    public Date getDate() {
        return date;
    }

    public List<Integer> getSeats() {
        return seats;
    }

    // For test purpose
    public boolean equal(Booking b) {
        return  b.getAmount() == this.amount &&
                this.date.equals(b.getDate()) &&
                this.customer == b.getCustomer();

    }

}
