package model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Testing DataAccess class")
public class DataAccessTest {
    @Test
    @DisplayName("Test data access object creation success")
    void dataAccessTest() {
        try {
            String testUrl ="jdbc:mysql://0.0.0.0:3306";
            String inDocker = System.getenv("IN_DOCKER");
            if (inDocker != null) {
                testUrl = "jdbc:mysql://database:3306";
            }
            DataAccess dataAccess = new DataAccess(testUrl, "root", "youshallnotpass");
            assertNotNull(dataAccess.getConnection());
        } catch (DataAccessException e) {
            fail(e.getMessage());
        } catch (Exception e) {
            fail(e.getMessage(), e.getCause());
        }
    }

    @Test()
    @DisplayName("Test data access object failure")
    void failDataAccessTest() {
        assertThrows(DataAccessException.class, DataAccess::new);
    }

    @Test()
    @DisplayName("Test initialisation method")
    void initialisationTest() {
        try {
            String sqlScript = "/home/titouan/git/java-ece-2017/database/database.sql";
            String testUrl ="jdbc:mysql://0.0.0.0:3306";
            String inDocker = System.getenv("IN_DOCKER");
            if (inDocker != null) {
                sqlScript = "/home/project/database/database.sql";
                testUrl = "jdbc:mysql://database:3306";
            }
            DataAccess dataAccess = new DataAccess(testUrl, "root", "youshallnotpass");
            assertNotNull(dataAccess.getConnection());
            dataAccess.initDatabase();
            dataAccess.initDatabase(sqlScript);
            dataAccess.initDatabase(sqlScript, sqlScript);
            assertEquals(1, 1);
        } catch (DataAccessException e) {
            fail(e.getMessage());
        } catch (Exception e) {
            fail(e.getMessage(), e.getCause());
        }
    }

    @Test()
    @DisplayName("Test get journey times method")
    void getJourneyTest() {
        try {
            String sqlScript = "/home/titouan/git/java-ece-2017/database/database.sql";
            String testUrl ="jdbc:mysql://0.0.0.0:3306/ece-train-test";
            String inDocker = System.getenv("IN_DOCKER");
            if (inDocker != null) {
                sqlScript = "/home/project/database/database.sql";
                testUrl = "jdbc:mysql://database:3306/ece-train-test";
            }
            DataAccess dataAccess = new DataAccess(testUrl, "root", "youshallnotpass");
            assertNotNull(dataAccess.getConnection());
            dataAccess.initDatabase(sqlScript);
            String fr = "2017-10-01";
            String too = "2017-12-01";
            Date from = Date.valueOf(fr);
            Date to =  Date.valueOf(too);

            Journey j1 = new Journey("paris", "amiens", 1, Date.valueOf("2017-10-09"), Date.valueOf("2017-10-09"));
            Journey j2 = new Journey("paris", "amiens", 1, Date.valueOf("2017-11-09"), Date.valueOf("2017-11-09"));
            Journey j3 = new Journey("paris", "amiens", 1, Date.valueOf("2017-11-10"), Date.valueOf("2017-11-10"));

            List<Journey> test = dataAccess.getTrainTimes("paris", "amiens", from, to );
            List<Journey> checkAll = new ArrayList<>();
            checkAll.add(j1);
            checkAll.add(j2);
            checkAll.add(j3);
            List<Journey> emptyCheck = new ArrayList<>();

            assertNotNull(test);
            assertNotEquals(checkAll, emptyCheck);
            assertNotEquals(checkAll, test); // Not equals cause they are not the same objects :'(
        } catch (DataAccessException e) {
            fail(e.getMessage());
        } catch (Exception e) {
            fail(e.getMessage(), e.getCause());
        }
    }

    @Test()
    @DisplayName("Test trip reservation")
    void makeReservation() {
        try {
            String sqlScript = "/home/titouan/git/java-ece-2017/database/database.sql";
            String testUrl ="jdbc:mysql://0.0.0.0:3306/ece-train-test";
            String inDocker = System.getenv("IN_DOCKER");
            if (inDocker != null) {
                sqlScript = "/home/project/database/database.sql";
                testUrl = "jdbc:mysql://database:3306/ece-train-test";
            }
            DataAccess dataAccess = new DataAccess(testUrl, "root", "youshallnotpass");
            assertNotNull(dataAccess.getConnection());

            dataAccess.initDatabase(sqlScript);
            Ticket t = dataAccess.buyTicket("paris", "amiens", Period.BLUE, 2, Class.FIRST);
            Ticket check = new Ticket("paris", "amiens", Period.BLUE, 2, Class.FIRST);
            assertNotNull(t);
            assertTrue(check.equal(t));
        } catch (DataAccessException e) {
            fail(e.getMessage());
        } catch (Exception e) {
            fail(e.getMessage(), e.getCause());
        }
    }

    @Test()
    @DisplayName("Book trip reservation")
    void makeBooking() {
        try {
            String sqlScript = "/home/titouan/git/java-ece-2017/database/database.sql";
            String testUrl ="jdbc:mysql://0.0.0.0:3306/ece-train-test";
            String inDocker = System.getenv("IN_DOCKER");
            if (inDocker != null) {
                sqlScript = "/home/project/database/database.sql";
                testUrl = "jdbc:mysql://database:3306/ece-train-test";
            }
            DataAccess dataAccess = new DataAccess(testUrl, "root", "youshallnotpass");
            assertNotNull(dataAccess.getConnection());
            dataAccess.initDatabase(sqlScript);

            Booking b = dataAccess.buyTicketAndBook(1, Date.valueOf("2017-10-09"), "paris", "amiens", 1, Class.FIRST, "test@test.com");
            Booking check = new Booking("","test@test.com", (float)27.375998,  Date.valueOf("2017-10-09"),new ArrayList<Integer>());
            assertNotNull(b);
            assertTrue(check.equal(b));
        } catch (DataAccessException e) {
            fail(e.getMessage());
        } catch (Exception e) {
            fail(e.getMessage(), e.getCause());
        }
    }

    @Test()
    @DisplayName("Delete booked trip reservation")
    void deleteBooking() {
        try {
            String sqlScript = "/home/titouan/git/java-ece-2017/database/database.sql";
            String testUrl ="jdbc:mysql://0.0.0.0:3306/ece-train-test";
            String inDocker = System.getenv("IN_DOCKER");
            if (inDocker != null) {
                sqlScript = "/home/project/database/database.sql";
                testUrl = "jdbc:mysql://database:3306/ece-train-test";
            }
            DataAccess dataAccess = new DataAccess(testUrl, "root", "youshallnotpass");
            assertNotNull(dataAccess.getConnection());
            dataAccess.initDatabase(sqlScript);

            Booking b = dataAccess.buyTicketAndBook(1, Date.valueOf("2017-10-09"), "paris", "amiens", 1, Class.FIRST, "george@test.com");
            Booking check = new Booking("","george@test.com", (float)27.375998,  Date.valueOf("2017-10-09"),new ArrayList<Integer>());
            assertNotNull(b);
            assertTrue(check.equal(b));
            assertTrue(dataAccess.cancelBooking(b.getId(), b.getCustomer()));

        } catch (DataAccessException e) {
            fail(e.getMessage());
        } catch (Exception e) {
            fail(e.getMessage(), e.getCause());
        }
    }


    @Test()
    @DisplayName("Get seat test")
    void getSeat() {
        try {
            String sqlScript = "/home/titouan/git/java-ece-2017/database/database.sql";
            String testUrl ="jdbc:mysql://0.0.0.0:3306/ece-train-test";
            String inDocker = System.getenv("IN_DOCKER");
            if (inDocker != null) {
                sqlScript = "/home/project/database/database.sql";
                testUrl = "jdbc:mysql://database:3306/ece-train-test";
            }
            DataAccess dataAccess = new DataAccess(testUrl, "root", "youshallnotpass");
            assertNotNull(dataAccess.getConnection());
            dataAccess.initDatabase(sqlScript);

            List<Integer> seats = dataAccess.getAvailableSeats(1, Date.valueOf("2017-12-26"), "paris", "amiens");
            List<Integer> check = new ArrayList<>();
            check.add(1);
            check.add(2);
            check.add(3);
            check.add(4);
            check.add(1);
            check.add(2);
            check.add(3);
            check.add(4);
            check.add(5);
            check.add(6);
            check.add(7);
            check.add(8);
            check.add(9);
            check.add(5);
            check.add(6);
            check.add(7);
            check.add(8);
            check.add(9);
            check.add(5);
            check.add(6);
            check.add(7);
            check.add(8);
            check.add(9);
            check.add(5);
            check.add(6);
            check.add(7);
            check.add(8);
            check.add(9);
            check.add(5);
            check.add(6);
            check.add(7);
            check.add(8);
            check.add(9);
            assertNotNull(seats);
            assertFalse(seats.isEmpty());
            check.sort(Comparator.naturalOrder());
            seats.sort(Comparator.naturalOrder());
            assertEquals(seats, check);

            Booking b = dataAccess.buyTicketAndBook(1, Date.valueOf("2017-10-09"), "paris", "amiens", 1, Class.FIRST, "george@test.com");
            Booking checkB = new Booking("","george@test.com", (float)27.375998,  Date.valueOf("2017-10-09"),new ArrayList<Integer>());
            assertNotNull(b);
            assertTrue(checkB.equal(b));

            seats = dataAccess.getAvailableSeats(1, Date.valueOf("2017-12-26"), "paris", "amiens");
            check.removeAll(b.getSeats());
            assertNotNull(seats);
            assertFalse(seats.isEmpty());
            seats.sort(Comparator.naturalOrder());
        } catch (DataAccessException e) {
            System.out.print(e);
            fail(e.getMessage());
        } catch (Exception e) {
            System.out.print(e.getMessage());
            fail(e.getMessage(), e.getCause());
        }
    }


    @Test()
    @DisplayName("Test close connection method")
    void closeConnectionTest() {
        try {
            String testUrl ="jdbc:mysql://0.0.0.0:3306";
            String inDocker = System.getenv("IN_DOCKER");
            if (inDocker != null) {
                testUrl = "jdbc:mysql://database:3306";
            }
            DataAccess dataAccess = new DataAccess(testUrl, "root", "youshallnotpass");
            dataAccess.close();
            assertNull(dataAccess.getConnection());
        } catch (DataAccessException e) {
            fail(e.getMessage());
        }
    }
}
